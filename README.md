# craft-avocado

Build a general config using entities that are distinguished using semantic versioning.

## Getting started

### Development

```sh
nvm use
```
```sh
npm ci
```

Build typescript in watch mode:
```sh
npm start
```

### Build Json Config

To write a json config file based on the versioned configurations in your project:
```sh
npm run build:json
```
