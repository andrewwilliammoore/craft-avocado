const factory = (version) => ({
  name: `whatchamacallit-${version}`,
  otherKey: 'for this version'
})

export default factory
