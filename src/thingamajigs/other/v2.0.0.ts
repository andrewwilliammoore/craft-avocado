const factory = (version) => ({
  name: `other-${version}`,
  extra: 'for this version'
})

export default factory
