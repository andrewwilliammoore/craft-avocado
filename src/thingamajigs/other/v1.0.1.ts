const factory = (version) => ({
  name: `other-${version}`
})

export default factory
