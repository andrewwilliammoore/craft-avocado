import { readdirSync, ensureDir, writeJson } from 'fs-extra'

const generateResources = async (buildDir) => {
  const resource = 'thingamajigs'
  const resourceDir = `${buildDir}/js/${resource}`
  const contents = await readdirSync(resourceDir, { withFileTypes: true })
  const entityDirs = contents.filter(c => c.isDirectory())

  const results = []
  await Promise.all(entityDirs.map(async dir => {
    const entity = dir.name
    const entityDir = `${resourceDir}/${entity}`
    const entityDirContents = await readdirSync(entityDir)
    // Modified from official semver matcher:
    // https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string
    const versioningMatcher = /^v(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?\.js$/
    const versionedFiles = entityDirContents.filter(c => c.match(versioningMatcher))

    versionedFiles.forEach(fileName => {
      const factory = require(`${entityDir}/${fileName}`).default
      const version = fileName.replace(/\.js$/, '')

      results.push(factory(version))
    })
  }))

  return {
    [resource]: results
  }
}

const generateAndWriteResources = async () => {
  const buildDir = `${process.cwd()}/build`
  const results = await generateResources(buildDir)

  const jsonBuildDir = `${buildDir}/json`
  await ensureDir(jsonBuildDir)

  const configFilePath = `${jsonBuildDir}/config.json`
  await writeJson(configFilePath, results)

  return configFilePath
}

generateAndWriteResources()
  .then((configFilePath) => {
    console.log('Finished. Config available at:', configFilePath)
  })
  .catch((e) => {
    console.log('Error:', e)
    process.exit(1)
  })
